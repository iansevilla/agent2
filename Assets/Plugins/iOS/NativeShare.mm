#import "NativeShare.h"

@implementation NativeShare

- (id)init
{
    self = [super init];
    
    return self;
}

-(bool) isFacebookAvailable
{   return [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
}

-(void) facebookShare: (const char*) msg
{
    
    if ([self isFacebookAvailable])
    {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText: [NSString stringWithUTF8String: msg]];
        [UnityGetGLViewController() presentViewController:facebookSheet animated:YES completion:nil];
    }
}

-(void) facebookShare: (const char*) msg withPhoto: (const char*) photoFilename
{
    
    if ([self isFacebookAvailable])
    {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText: [NSString stringWithUTF8String: msg]];
        
       UIImage *image = [UIImage imageWithContentsOfFile: [NSString stringWithUTF8String: photoFilename]];
        
        [facebookSheet addImage: image];
        
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        [UnityGetGLViewController() presentViewController:facebookSheet animated:YES completion:nil];
    }
}

-(void) facebookShare: (const char*) msg withLink: (const char*) link
{
    
    if ([self isFacebookAvailable])
    {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText: [NSString stringWithUTF8String: msg]];
        
        [facebookSheet addURL: [NSURL URLWithString: [NSString stringWithUTF8String: link]]];
        
        [UnityGetGLViewController() presentViewController:facebookSheet animated:YES completion:nil];
    }
}


-(void) facebookShare: (const char*) msg withPhoto: (const char*) photoFilename withLink: (const char*) link
{
    
    if ([self isFacebookAvailable])
    {
        SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [facebookSheet setInitialText: [NSString stringWithUTF8String: msg]];
        
        UIImage *image = [UIImage imageWithContentsOfFile: [NSString stringWithUTF8String: photoFilename]];
        
        [facebookSheet addImage: image];
        
        [facebookSheet addURL: [NSURL URLWithString: [NSString stringWithUTF8String: link]]];
        
        [UnityGetGLViewController() presentViewController:facebookSheet animated:YES completion:nil];
    }
}

-(bool) isTwitterAvailable
{   return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}

-(void) tweet: (const char*) msg
{
    if ([self isTwitterAvailable])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText: [NSString stringWithUTF8String: msg]];
        [UnityGetGLViewController() presentViewController:tweetSheet animated:YES completion:nil];
    }
}

-(void) tweet: (const char*) msg withPhoto: (const char*) photoFilename
{
    if ([self isTwitterAvailable])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText: [NSString stringWithUTF8String: msg]];
        
        UIImage *image = [UIImage imageWithContentsOfFile: [NSString stringWithUTF8String: photoFilename]];
        
        [tweetSheet addImage: image];
        
        [UnityGetGLViewController() presentViewController:tweetSheet animated:YES completion:nil];
    }
}

-(void) tweet: (const char*) msg withLink: (const char*) link
{
    if ([self isTwitterAvailable])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText: [NSString stringWithUTF8String: msg]];
        [tweetSheet addURL: [NSURL URLWithString: [NSString stringWithUTF8String: link]]];
        
        [UnityGetGLViewController() presentViewController:tweetSheet animated:YES completion:nil];
    }
}

-(void) tweet: (const char*) msg withPhoto: (const char*) photoFilename withLink: (const char*) link
{
    if ([self isTwitterAvailable])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText: [NSString stringWithUTF8String: msg]];
        
        UIImage *image = [UIImage imageWithContentsOfFile: [NSString stringWithUTF8String: photoFilename]];
        
        [tweetSheet addImage: image];
        
        [tweetSheet addURL: [NSURL URLWithString: [NSString stringWithUTF8String: link]]];
        
        [UnityGetGLViewController() presentViewController:tweetSheet animated:YES completion:nil];
    }
}

@end

static NativeShare *nativeSharePlugin = nil;

extern "C"
{
    bool _IsFacebookAvailable()
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        return [nativeSharePlugin isFacebookAvailable];
    }
    
    void _FacebookShare(const char* msg)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin facebookShare: msg];
    }
    
    void _FacebookShareWithPhoto(const char* msg, const char* photoFilename)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin facebookShare: msg withPhoto: photoFilename];
    }
    
    void _FacebookShareWithLink(const char* msg, const char* url)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin facebookShare: msg withLink: url];
    }
    
    void _FacebookShareWithPhotoAndLink(const char* msg, const char* photoFilename, const char *link)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin facebookShare: msg withPhoto: photoFilename withLink: link];
    }
    
    bool _IsTwitterAvailable()
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        return [nativeSharePlugin isTwitterAvailable];
    }
    
    void _Tweet(const char* msg)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin tweet: msg];
    }
    
    void _TweetWithPhoto(const char* msg, const char* photoFilename)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin tweet: msg withPhoto: photoFilename];
    }
    
    void _TweetWithLink(const char* msg, const char* link)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin tweet: msg withLink: link];
    }
    
    void _TweetWithPhotoAndLink(const char* msg, const char* photoFilename, const char* link)
    {
        if (nativeSharePlugin == nil)
			nativeSharePlugin = [[NativeShare alloc] init];
        
        [nativeSharePlugin tweet: msg withPhoto: photoFilename withLink: link];
    }
}