﻿using UnityEngine;
using System.Collections;

public class TestScene : OGameScene 
{
	public OTextButton m_backButton;
	
	public virtual void Start()
	{	
		if(m_backButton != null)
		{	m_backButton.isAndroidBackButton = true;
		}	else
		{	Debug.LogWarning("Assign BackButton!");
		}
	}

	public override void OnTouchUp() 
	{	base.OnTouchUp();

		if(m_backButton.isTouched())
		{	m_backButton.OnUp();
			returnToTestScenesMenu();
		}
	}

	public override void OnKeycodeEscape()
	{	for(int i=0; i< m_buttons.Count; i++)
		{	OButton button = m_buttons[i];
			if(button.isAndroidBackButton)
			{	returnToTestScenesMenu();
			}
		}
	}

	void returnToTestScenesMenu()
	{	Debug.Log("Returning to TestScenesMenu...");
		OGameMgr.Instance.PushScene("TestScenesMenu");
	}
}
