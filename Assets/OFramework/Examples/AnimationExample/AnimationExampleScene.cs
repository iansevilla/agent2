﻿using UnityEngine;
using System.Collections;

public class AnimationExampleScene : OGameScene {

	// Use this for initialization
	void Start () {
		OAnimation.PlayAnimationsInChildren(gameObject, "default");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
