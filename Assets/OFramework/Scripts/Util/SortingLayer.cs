﻿using UnityEngine;
using System.Collections;

public class SortingLayer : MonoBehaviour {

	public static string NameOf(SORTING_LAYER layer) {
		switch(layer)
		{
		default:
		case SORTING_LAYER.DEFAULT :
			return "Default";
		case SORTING_LAYER.UI :
			return "UI";
		case SORTING_LAYER.POPUP :
			return "Popup";
		case SORTING_LAYER.NOTIFICATION :
			return "Notification";
		}
	}
}
