﻿using UnityEngine;
using System.Collections;

public class SolidSprite : MonoBehaviour {

	//TEXTURE SETTINGS
	const int texWidth = 32;
	
	static Texture2D _texture = null;
	
	static Texture2D texture
	{
		get {
			if (_texture == null) {
				Texture2D tex = new Texture2D(texWidth, texWidth, TextureFormat.RGBA32, true);
				
				tex.wrapMode = TextureWrapMode.Repeat;
				
				for(int y = 0; y <= texWidth ; y++)
				{
					for(int x = 0; x <= texWidth; ++x)
					{
						tex.SetPixel(x, y, Color.white);
					}
				}
				
				tex.Apply();
				
				_texture = tex;
			}
			
			return _texture;
		}
	}
	
	
	static Sprite _sprite = null;
	public static Sprite sprite
	{
		get {
			if (_sprite == null) {
				_sprite = Sprite.Create(texture, new Rect(0, 0, texWidth, texWidth), new Vector2(0.5f, 0.5f), texWidth);
			}
			
			return _sprite;
		}
	}
}
