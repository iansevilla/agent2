﻿using UnityEngine;
using System.Collections;

public class OScaleAnimation : OAnimation {

	public Vector3 initialScale = Vector3.one;
	public Vector3 targetScale = Vector3.one;
	
	public override void ResetAnimation ()
	{
		gameObject.transform.localScale = initialScale;
	}

	public override void DoAnimation ()
	{
		ApplyBaseSettings(LeanTween.scale (gameObject, targetScale, animationDuration));
	}
}
