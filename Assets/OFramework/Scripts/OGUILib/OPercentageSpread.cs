﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class OPercentageSpread : MonoBehaviour 
{
	public float m_xPercentage = 0.5f;
	public float m_yPercentage = 0.5f;
	
	void Start () 
	{	
		setPercentageSpread (this.transform, m_xPercentage, m_yPercentage);
	}
	
	public static void setPercentageSpread(Transform objectTransform, float xPercentage, float yPercentage)
	{	
		Vector3 newPos = Camera.main.ScreenToWorldPoint( new Vector3( OResolutionHandler.screenWidth () * xPercentage , OResolutionHandler.screenHeight () * yPercentage, 0f ) ); 
		objectTransform.localPosition = new Vector3( newPos.x, newPos.y, 0 );
	}

	public void Update() 
	{	
		if (Application.isEditor && !Application.isPlaying)
		{	setPercentageSpread (this.transform, m_xPercentage, m_yPercentage);
		}
	}
}
