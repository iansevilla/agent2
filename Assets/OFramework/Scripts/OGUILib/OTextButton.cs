﻿using UnityEngine;
using System.Collections;

public class OTextButton : OButton
{
	public TextMesh m_textLabel;
	
	public override void Awake()
	{
		base.Awake ();

		initTextLabel();
		m_buttonColor = m_textLabel.color;	

		m_textLabel.offsetZ = -1;

		Disable (isDisabled);
	}

	public override void OnDown()
	{	base.OnDown();	
		
		m_textLabel.color = new Color( 0.7f, 0.7f, 0.7f);
	}

	public override void OnUp()
	{	base.OnUp();	
	
		m_textLabel.color = m_buttonColor;	
	}

	public override void Disable(bool isDisabled)
	{	base.Disable(isDisabled);
		
		initTextLabel();

		if(isDisabled)
		{	m_textLabel.color = new Color( 0.7f, 0.7f, 0.7f);
		}	else
		{	m_textLabel.color = m_buttonColor;	
		}
	}

	void initTextLabel()
	{	if(m_textLabel == null)
		{
			if(gameObject.GetComponent<TextMesh>() != null)
			{	m_textLabel = gameObject.GetComponent<TextMesh>();
			}	else
			{	Debug.LogWarning("OTextButton :: Assign TextLabel!");
			}
		}
	}
}