using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class OAppStoreHandler : Singleton<OAppStoreHandler> 
{
	#if UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _OpenAppInStore(int appID);
	#endif
	
	#if UNITY_ANDROID
	private static AndroidJavaObject jo;
	#endif

	public delegate void Action();
	public static Action OnAppStoreClosed = delegate {};
	
	bool isOpen = false;
	
	public void InitMgr()
	{
		if(!Application.isEditor)
		{
			#if UNITY_ANDROID
			jo = new AndroidJavaObject("com.orangenose.nativeandroid.AppStore");
			#endif
			
		}	else
		{	Debug.Log("OAppStoreHandler:: Cannot open App Store in Editor.");
		}
	}

	public void openAppInStore(string appID)
	{
		isOpen = true;
		
		if(!Application.isEditor)
		{
			#if UNITY_IPHONE
			int appIDIOS;

			if(int.TryParse(appID, out appIDIOS))
			{	_OpenAppInStore(appIDIOS);
			}
			#endif
			
			#if UNITY_ANDROID
			jo.Call("OpenInAppStore", "market://details?id="+appID);
			#endif
		}	else
		{	Debug.Log("OAppStoreHandler:: Cannot open App Store in Editor.");
		}
	}

	void appStoreClosed()
	{	
		#if UNITY_IPHONE
		Debug.Log("OAppStoreHandler:: App Store closed.");
		isOpen = false;
		OnAppStoreClosed();
		#endif
	}

	void OnApplicationPause(bool pauseStatus)
	{	
		#if UNITY_ANDROID
		if(!pauseStatus && isOpen)
		{	Debug.Log("OAppStoreHandler:: App Store closed.");
			isOpen = false;
			OnAppStoreClosed();
		}
		#endif
	}
}	
