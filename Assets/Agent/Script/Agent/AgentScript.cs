﻿using UnityEngine;
using System.Collections;

public class AgentScript : MonoBehaviour {

	public static AgentScript instance;

	//Sprite
	public SpriteRenderer AgentSprite;

	//Alive
	private bool m_IsAliveAgent;

	//ShootSpeed
	private float m_timeToShoot;
	private float m_maxTimeToShoot;

	Animator Anim;

	//AgentAim
	public bool m_AgentShootOnce = false;

	//AgentDie
	public bool m_AgentDieOnce = false;

	//Audio
	public	AudioClip m_sfx_shoot;
	public 	AudioClip m_sfx_shout;

	//headshot
	public GameObject go_HeadShotCol;

	void Start () 
	{
		instance 			= this;

		m_IsAliveAgent 		= true;

		Anim = this.gameObject.GetComponent<Animator>();

		m_timeToShoot = Random.Range (AgentConfig.AGENT_SHOOT_SPEED, AgentConfig.AGENT_MAX_SHOOT_SPEED);
		//m_maxTimeToShoot 	= AgentConfig.AGENT_MAX_SHOOT_SPEED;

	}

	void Update () 
	{
		if(!m_IsAliveAgent && !m_AgentDieOnce)
		{
			m_AgentDieOnce = true;
			Anim.SetTrigger("AgentDie");
			OAudioMgr.Instance.PlaySFX(m_sfx_shout);
			StartCoroutine( AgentStartToDestroy());
		}
	}

	void FixedUpdate()
	{
		AgentShot();
	}

	void LateUpdate()
	{
		if(!GridManager.instance.Get_IsGameStart())
		{
			Destroy(this.gameObject);
		}
	}
	
	public void AgentShot()
	{
		if(!m_IsAliveAgent)
			return;

		if(!PlayerScript.instance.Get_IsAlivePlayer())
			return;

		m_timeToShoot -= Time.deltaTime;
		
		if (m_timeToShoot <= 0 && !m_AgentShootOnce)
		{
			if(PlayerScript.instance.Get_IsAlivePlayer())
			{
				OAudioMgr.Instance.PlaySFX(m_sfx_shoot);
				OGameMgr.Instance.Fader.Fade(Color.red, 0f, .5f, .2f, deactivateOnFadeEnd: false, onFadeEnd: OnFadeEndAgent);
				PlayerScript.instance.Set_IsAlivePlayer(false);

			}
			m_AgentShootOnce = true;
		}
		else if(m_timeToShoot <= .3)
			Anim.SetTrigger("AgentShoot");

	}

	void OnFadeEndAgent()
	{
		OGameMgr.Instance.Fader.Fade (Color.red, .5f, 0f, .2f);

		foreach (GameObject go in GameObject.FindGameObjectsWithTag("Agent"))
		{
			go.GetComponent<Animator>().enabled = false;
			Color AgentColor = go.renderer.material.color;
			AgentColor.a = 0.3f;
			go.renderer.material.color = AgentColor;

		}
		AgentSprite.GetComponent<Animator>().enabled = false;
		Color thisAgentColor = AgentSprite.renderer.material.color;
		thisAgentColor.a = 1f;
		AgentSprite.material.color = thisAgentColor;

		PlayerScript.instance.ShowReload (false);

		StartCoroutine (StartMovingToRetry ());
		//GridManager.instance.Set_IsGameStart(false);
		//OGameMgr.Instance.ShowPopup("SResultPopup");
	}
	IEnumerator StartMovingToRetry()
	{
		yield return new WaitForSeconds (2f);
		GridManager.instance.Set_IsGameStart(false);
		OGameMgr.Instance.ShowPopup("SResultPopup");
	}
	
	IEnumerator AgentStartToDestroy()
	{

		yield return new WaitForSeconds (.7f);
		AgentSprite.GetComponent<Animator>().enabled = false;

		Color AgentColor = AgentSprite.renderer.material.color;
		while(AgentColor.a > 0)
		{
			AgentColor.a -= 0.2f;
			AgentSprite.renderer.material.color = AgentColor;
			yield return new WaitForSeconds (.1f);
		}

		yield return new WaitForSeconds (.2f);
		GridManager.instance.ObjectSelfDestroyReset (this.gameObject);
		Destroy (this.gameObject);
	}

	public bool Get_IsAliveAgent()
	{	return m_IsAliveAgent;
	}

	public void Set_IsAliveAgent(bool isAlive)
	{	m_IsAliveAgent = isAlive;
		if (!isAlive)
		{
			this.collider.enabled = isAlive;
			go_HeadShotCol.SetActive(false);
		}
	}
}
