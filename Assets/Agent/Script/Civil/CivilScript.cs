﻿using UnityEngine;
using System.Collections;

public class CivilScript : MonoBehaviour {

	public static CivilScript instance;

	public GameObject WarningSign;

	private bool m_IsAliveCivil;

	//Audio
	public AudioClip m_sfx_Scream;

	public bool m_bCivilDieOnce = false;

	void Start () {

		instance 			= this;

		m_IsAliveCivil		= true;
		StartCoroutine (DestroySelf());
		WarningSign.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if(!m_IsAliveCivil && !m_bCivilDieOnce)
		{
			m_bCivilDieOnce = true;
			StopCoroutine("DestroySelf");
			StartCoroutine(CivilStartToDestroy());
			OAudioMgr.Instance.PlaySFX(m_sfx_Scream);
			WarningSign.SetActive(true);
		}
	}
	void LateUpdate()
	{
		if(!GridManager.instance.Get_IsGameStart())
		{
			Destroy(this.gameObject);
		}
	}
	
	public bool Get_IsAliveCivil()
	{	return m_IsAliveCivil;
	}

	public void Set_IsAliveCivil(bool isAlive)
	{	m_IsAliveCivil = isAlive;
	}

	IEnumerator CivilStartToDestroy()
	{
		yield return new WaitForSeconds (.3f);
		Destroy (this.gameObject);
	}

	IEnumerator DestroySelf()
	{
		yield return new WaitForSeconds (3f);
		GridManager.instance.ObjectSelfDestroyReset(this.gameObject);
		Destroy (this.gameObject);
	}
}
