﻿using UnityEngine;
using System.Collections;

public class SelfDestroy : MonoBehaviour {

	public float DestroyTime = 1f; 
	public bool m_isFading = true;
	// Use this for initialization
	void Start () {
		StartCoroutine (DestroySelfTime(DestroyTime));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator DestroySelfTime(float desTime)
	{
		if(m_isFading)
		{
			Color thisColor = this.renderer.material.color;
			while(thisColor.a > 0)
			{
				thisColor.a -= 0.05f;
				this.renderer.material.color = thisColor;
				yield return new WaitForSeconds (.2f);
			}
		}
		yield return new WaitForSeconds(desTime);
		Destroy (this.gameObject);
	}
}
