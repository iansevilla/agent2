﻿using UnityEngine;
using System.Collections;

public class GlobalClass : MonoBehaviour {

	public static GlobalClass instance;

	private bool m_bIsFromMainMenu = false;

	// Use this for initialization
	void Start () {
		instance = this;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Set_IsFromMainMenu(bool isFromMainMenu)
	{	m_bIsFromMainMenu = isFromMainMenu;
	}
	public bool Get_IsFromMainMenu()
	{	return m_bIsFromMainMenu;
	}
}
