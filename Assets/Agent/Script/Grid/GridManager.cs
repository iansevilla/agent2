﻿using UnityEngine;
using System.Collections;


public class GridManager : MonoBehaviour {

	public static GridManager instance;

	private bool m_bIsGameStart = false;
	
	//Grid
	private int m_width = 4;
	private int m_height = 4;

	private int[] LevelArray = {	
								1, 1, 1, 1, 
								1, 1, 1, 1, 
								1, 1, 1, 1,
								1, 1, 1, 1,
								};
							
	public GameObject AgentPrefab;
	public GameObject CivilPrefab;

	public GameObject BulletHolePrefab;
	public GameObject BulletHoldBloodPrefab;
	public GameObject BulletHoleHeadShot;
	public GameObject ScoreTextPrefab;

	public GameObject[,] pieces;

	//Audio
	public AudioClip m_sfx_Shoot;
	public AudioClip m_sfx_OutOfBullet;



	public float SpawnTimer;

	public float SpawnRoundTimer;
	public float ResetGridTimer;

	public int	m_playerRound = 0;
	
	void Awake()
	{

	}


	void Start () {

		instance 			= this;

		pieces 				= new GameObject[m_width, m_height];
		SpawnTimer 			= GameConfig.SPAWN_TIMER;
		ResetGridTimer 		= GameConfig.GRID_RESET_TIMER;
	}
	
	void Update () {
		if (!PlayerScript.instance.Get_IsAlivePlayer())
			return;		
		
		if(m_bIsGameStart)
		{

			MouseClick ();
			//TapSelect ();

			//SpawnPerSec ();
			//ResetGrid ();

			//SpawnAgentRound(6, 1, 0, 1f);
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			m_bIsGameStart = true;
			SetSpawnRound(1);
			//SpawnAgent();
		}
		if(Input.GetKeyDown(KeyCode.R))
		{
			PlayerScript.instance.Set_PlayerBullets(PlayerConfig.PLAYER_BULLETS);
		}
		if(Input.GetKey(KeyCode.P))
		{
			m_bIsGameStart = true;
			SpawnAgent();
		}
	}

	void FixedUpdate()
	{
	
	}

	public void ResetGrid()
	{
		for(int x = 0; x<m_width; x++)
		{
			for(int y = 0; y < m_height; y++)
			{
				if(LevelArray[ y*m_height + x] == 0)
				{
					LevelArray[ y*m_height + x] = 1;
				}
			}
		}


	}
	
	void SpawnPerSec()
	{	SpawnTimer -= Time.deltaTime;
		if(SpawnTimer <=0)
		{	
			if(Random.Range(0,100)>5)
				SpawnAgent();
			else
				SpawnCivil();


			if(PlayerScript.instance.Get_PlayerScore() >= 30)
				SpawnTimer = .3f;
			else if(PlayerScript.instance.Get_PlayerScore() >= 50)
				SpawnTimer = .1f;
			else if(PlayerScript.instance.Get_PlayerScore() >= 100)
				SpawnTimer = .05f;
			else
				SpawnTimer = .5f;
		}
	}


	IEnumerator SpawnAgentRound(int nSpawnAgent, int nSpawnCivil, int nSpawnObject, float SpawnRate)
	{
		ResetGrid ();
		if (!PlayerScript.instance.Get_IsAlivePlayer())
			StopAllCoroutines ();

		while(nSpawnAgent > 0 || nSpawnCivil > 0)
		{
			if (!PlayerScript.instance.Get_IsAlivePlayer())
				StopAllCoroutines ();

			if(nSpawnCivil <= 0)
			{	SpawnAgent();
				nSpawnAgent--;
			}
			else
			{
				if(Random.Range(0,3)== 0)
				{	SpawnCivil();
					nSpawnCivil--;
				}
				else
				{	SpawnAgent();
					nSpawnAgent--;
				}
			}
			yield return new WaitForSeconds(SpawnRate);
		}

		if (nSpawnAgent <= 0 && nSpawnCivil <= 0)
		{	yield return new WaitForSeconds(SpawnRate + Random.Range(0.2f, 1f));
			m_playerRound += 1;
			SetSpawnRound(m_playerRound);
		}
	}

	public void SetSpawnRound(int nRound)
	{
		Debug.Log ("ROUND: " + nRound);
		switch(nRound)
		{
		case 0:
			StartCoroutine(SpawnAgentRound(6, 0, 0, .8f));
			break;
		case 1:
			StartCoroutine(SpawnAgentRound(6, 1, 0, .8f));
			break;
		case 2:
		case 3:
			StartCoroutine(SpawnAgentRound(8, 2, 0, 0.6f));
			break;
		case 4:
			StartCoroutine(SpawnAgentRound(15, 3, 0, 0.6f));
			break;
		case 5:
		case 6:
		case 7:
			StartCoroutine(SpawnAgentRound(15, 4, 0, 0.4f));
			break;
		case 8:
		case 9:
		case 10:
			StartCoroutine(SpawnAgentRound(15, 4, 0, 0.3f));
			break;
		case 11:
			StartCoroutine(SpawnAgentRound(18, 4, 0, 0.2f));
			break;
		case 12:
		case 13:
		case 14:
		case 15:
			StartCoroutine(SpawnAgentRound(15, 4, 0, 0.4f));
			break;
		case 16:
			StartCoroutine(SpawnAgentRound(25, 4, 0, 0.2f));
			break;
		case 17:
		case 18:
			StartCoroutine(SpawnAgentRound(15, 4, 0, 0.3f));
			break;
		default:
			StartCoroutine(SpawnAgentRound(25, 4, 0, 0.2f));
			break;
		}

	}

	void SpawnAgent()
	{
		if (!PlayerScript.instance.Get_IsAlivePlayer())
			return;
		int RandomX = Random.Range (0,4);
		int RandomY = Random.Range (0,4);
		//Debug.Log ("RandomX: " + RandomX);
		//Debug.Log ("RandomY: " + RandomY);
		//Debug.Log ("ARRAY INDEX Agent: " + RandomY * m_height + RandomX);
		if(LevelArray[ RandomY*m_height + RandomX] == 1)
		{
			pieces [RandomX,RandomY] = (GameObject)Instantiate(AgentPrefab, new Vector3(GetTrasformX(RandomX),GetTransformY(-RandomY+ m_height),0),
			                                                   Quaternion.identity);
			pieces [RandomX,RandomY].name = "Agent" + RandomX.ToString("D2") + RandomY.ToString("D2");
			LevelArray[ RandomY*m_height + RandomX] = 0;
		}


		else if(LevelArray[ RandomY*m_height + RandomX] == 0)
		{
			SpawnAgent();
		}
	}

	void SpawnCivil()
	{
		if (!PlayerScript.instance.Get_IsAlivePlayer())
			return;

		int RandomX = Random.Range (0,4); 
		int RandomY = Random.Range (0,4);
		//Debug.Log ("ARRAY INDEX Agent: " + RandomY * m_height + RandomX);
		if(LevelArray[ RandomY*m_height + RandomX] == 1)
		{
			pieces [RandomX,RandomY] = (GameObject)Instantiate(CivilPrefab, new Vector3(GetTrasformX(RandomX),GetTransformY(-RandomY+ m_height),0),
			                                                   Quaternion.identity);
			pieces [RandomX,RandomY].name = "Civil" + RandomX.ToString("D2") + RandomY.ToString("D2");
			LevelArray[ RandomY*m_height + RandomX] = 0;
		}
		else if(LevelArray[ RandomY*m_height + RandomX] == 0)
		{
			SpawnCivil();
		}
	}

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// TouchEvents ////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void MouseClick() 
	{	if (Input.GetMouseButtonDown(0)) 
		{	Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit ;
			if (Physics.Raycast (ray, out hit))
				ObjectTouchEvents(hit, hit.point.x, hit.point.y);
	}	}
	
	void TapSelect() 
	{	foreach (Touch touch in Input.touches) 
		{	if ( touch.phase == TouchPhase.Began) 
			{	Ray ray = Camera.main.ScreenPointToRay(touch.position);
				RaycastHit hit ;
				if (Physics.Raycast (ray, out hit)) 
					ObjectTouchEvents(hit, hit.point.x,  hit.point.y);
	}	}	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// TouchObject ////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	void ObjectTouchEvents(RaycastHit hit, float HitPosX, float HitPosY)
	{	if (PlayerScript.instance.Get_IsPlayerOutOfBullet ())
		{
			PlayerScript.instance.ShowReload(true);
			PlayerScript.instance.Set_PlayerMultiplier(1, true);
			OAudioMgr.Instance.PlaySFX(m_sfx_OutOfBullet);
			return;
		}
		int currentBullet = PlayerScript.instance.Get_PlayerBullets ();
		currentBullet -= 1;
		PlayerScript.instance.Set_PlayerBullets (currentBullet);

		if(hit.collider.tag == "Agent")
		{ 	ShootAgent(hit.collider.gameObject, false);
			Instantiate (BulletHoldBloodPrefab, new Vector2 (HitPosX, HitPosY), Quaternion.identity);
			GameObject scoreObject = Instantiate(ScoreTextPrefab, new Vector2 (hit.collider.gameObject.transform.position.x,
			                                                                         hit.collider.gameObject.transform.position.y),
			                                     									Quaternion.identity) as GameObject;
			scoreObject.GetComponent<ScoringText>().Set_Score(AgentConfig.AGENT_SCORE * PlayerScript.instance.Get_PlayerMultiplier());
			//PlayerScript.instance.SetIsFromMissed(false);
			OAudioMgr.Instance.PlaySFX (m_sfx_Shoot);
		}

		else if(hit.collider.tag == "AHeadShot")
		{ 	
			Debug.Log("@@@@@@@@@@HEADSHOT@@@@@@@@@@@@");
			ShootAgent(hit.collider.gameObject.transform.parent.gameObject, true);
			Instantiate (BulletHoleHeadShot, new Vector2 (HitPosX, HitPosY), Quaternion.identity);
			GameObject scoreObject = Instantiate(ScoreTextPrefab, new Vector2 (hit.collider.gameObject.transform.position.x,
			                                                                   hit.collider.gameObject.transform.position.y),
			                                     Quaternion.identity) as GameObject;
			scoreObject.GetComponent<ScoringText>().Set_Score(AgentConfig.AGENT_HEADSHOOT_SCORE * PlayerScript.instance.Get_PlayerMultiplier());
			//PlayerScript.instance.SetIsFromMissed(false);
			OAudioMgr.Instance.PlaySFX (m_sfx_Shoot);
		}

		else if(hit.collider.tag == "Civil")
		{ 	ShootCivil(hit.collider.gameObject);
			PlayerScript.instance.Set_PlayerMultiplier(1, true);
			Instantiate (BulletHoldBloodPrefab, new Vector2 (HitPosX, HitPosY), Quaternion.identity);
			LevelArray[GetArrayY((int)hit.collider.gameObject.transform.position.y) * m_height + 
			           GetArrayX((int)hit.collider.gameObject.transform.position.x)] = 1;
			OAudioMgr.Instance.PlaySFX (m_sfx_Shoot);
		}
		else if(hit.collider.tag == "Missed")
		{ 	Instantiate (BulletHolePrefab, new Vector2 (HitPosX, HitPosY), Quaternion.identity);
			PlayerScript.instance.Set_PlayerMultiplier(1, true);
			OAudioMgr.Instance.PlaySFX (m_sfx_Shoot);
		}
		else if(hit.collider.tag == "btn_Pause")
		{
			OGameMgr.Instance.SetPaused(true);
			OGameMgr.Instance.ShowPopup("SPausePopup");
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// KILLED THE OBJECT //////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void ShootAgent(GameObject go_AgentIndex, bool isHeadShot = false)
	{	if(go_AgentIndex.GetComponent<AgentScript>().Get_IsAliveAgent())
		{
			int AgentScore;
			if(isHeadShot)
				AgentScore = AgentConfig.AGENT_HEADSHOOT_SCORE;
			else
				AgentScore = AgentConfig.AGENT_SCORE;
			PlayerScript.instance.Set_PlayerScore(AgentScore * PlayerScript.instance.Get_PlayerMultiplier());

			go_AgentIndex.GetComponent<AgentScript>().Set_IsAliveAgent(false);
		}
	}

	public void ShootCivil(GameObject go_CivilIndex)
	{	if(go_CivilIndex.GetComponent<CivilScript>().Get_IsAliveCivil())
		{	PlayerScript.instance.Set_PlayerScore(CivilConfig.CIVIL_SCORE);
			go_CivilIndex.GetComponent<CivilScript>().Set_IsAliveCivil(false);
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// SET GET GAME START ////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public bool Get_IsGameStart()
	{	return m_bIsGameStart;
	}

	public void Set_IsGameStart(bool isGameStart)
	{	m_bIsGameStart = isGameStart;
	}
	

	public void ObjectSelfDestroyReset(GameObject go_Object)
	{
		LevelArray[GetArrayY((int)go_Object.transform.position.y) * m_height + 
		           GetArrayX((int)go_Object.transform.position.x)] = 1;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// GET COORDINATE FOR THE GRID ////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public int GetTrasformX(int x)
	{	int nNewX = 0;
		switch(x){
		case 0:
			nNewX = GameConfig.GRID_X_1;
			break;
		case 1:
			nNewX = GameConfig.GRID_X_2;
			break;
		case 2:
			nNewX = GameConfig.GRID_X_3;
			break;
		case 3:
			nNewX = GameConfig.GRID_X_4;
			break;
		default:
			break;
		}
		return nNewX;
	}
	
	public int GetTransformY(int y)
	{	int nNewY = 0;
		//Debug.Log("TRANSFORM Y: " + y);
		switch(y){
		case 1:
			nNewY = GameConfig.GRID_Y_1;
			break;
		case 2:
			nNewY = GameConfig.GRID_Y_2;
			break;
		case 3:
			nNewY = GameConfig.GRID_Y_3;
			break;
		case 4:
			nNewY = GameConfig.GRID_Y_4;
			break;
		default:
			break;
		}
		return nNewY;
	}

	public int GetArrayX(int x)
	{	int nArrayX = 0;
		switch(x){
		case GameConfig.GRID_X_1:
			nArrayX = 0;
			break;
		case GameConfig.GRID_X_2:
			nArrayX = 1;
			break;
		case GameConfig.GRID_X_3:
			nArrayX = 2;
			break;
		case GameConfig.GRID_X_4:
			nArrayX = 3;
			break;
		default:
			break;
		}
		return nArrayX;
	}

	public int GetArrayY(int y)
	{	int nArrayY = 0;
		switch(y){
		case GameConfig.GRID_Y_1:
			nArrayY = 3;
			break;
		case GameConfig.GRID_Y_2:
			nArrayY = 2;
			break;
		case GameConfig.GRID_Y_3:
			nArrayY = 1;
			break;
		case GameConfig.GRID_Y_4:
			nArrayY = 0;
			break;
		default:
			break;
		}
		return nArrayY;
	}

	public void Set_CurrentRound(int nRound)
	{	m_playerRound = nRound;
	}

	public int Get_CurrentRound()
	{	return m_playerRound;
	}
}
