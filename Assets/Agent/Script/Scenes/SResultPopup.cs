using UnityEngine;
using System.Collections;

public class SResultPopup : OPopupScene {

	public OLabel lblScore;
	public OLabel lblBestScore;

	public AudioClip m_bgm_Result;
	public AudioClip m_bgm_InGame;

	public OSpriteToggleButton m_toggleBGM;
	public OSpriteToggleButton m_toggleSFX;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		lblScore.UpdateLabel (PlayerScript.instance.Get_PlayerScore ());
		lblBestScore.UpdateLabel ("Best: " + ODBMgr.Instance.getInt (ODBKeys.GAME_BEST_SCORE) + "pts.");
	}

	void OnEnable()
	{
		//OInMobiHandler.Instance.createBannerAd();

		OAudioMgr.Instance.PlayBGM (m_bgm_Result);
		
		m_toggleBGM.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_BGM_ENABLED ) );
		m_toggleSFX.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_SFX_ENABLED ) );
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_retry")
		{	
			PlayerScript.instance.Set_IsAlivePlayer(true);
			PlayerScript.instance.ResetPlayerScore();
			PlayerScript.instance.ShowReload(false);
			PlayerScript.instance.ShowSwipeReload(true);
			PlayerScript.instance.HideAllBullet();
			GridManager.instance.ResetGrid();
			GridManager.instance.Set_CurrentRound(0);
			OAudioMgr.Instance.PlayBGM (m_bgm_InGame);
			SGameScene.instance.Set_IsInGame (true);
			OGameMgr.Instance.HidePopup();
		}

		else if (button.m_buttonId == "btn_MainMenu")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
		}

		else if (button.m_buttonId == "btn_bgm")
		{	OAudioMgr.Instance.SetEnabledBGM (m_toggleBGM.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_BGM_ENABLED, m_toggleBGM.m_isToggled );
		}
		
		else if (button.m_buttonId == "btn_sfx")
		{	OAudioMgr.Instance.SetEnabledSFX (m_toggleSFX.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_SFX_ENABLED, m_toggleSFX.m_isToggled );
		}
		
		else if (button.m_buttonId == "btn_aboutScreen")
		{	OGameMgr.Instance.ShowPopup("SAboutPopup");
		}
	}
}
