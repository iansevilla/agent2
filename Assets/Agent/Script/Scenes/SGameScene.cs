﻿using UnityEngine;
using System.Collections;

public class SGameScene : OGameScene {
	
	public static SGameScene instance;

	public AudioClip m_sfx_reload;

	private bool m_IsInGame;

	void Start () {
		instance = this;
		OGameMgr.Instance.ShowPopup("SSplashPopup");
	}
	

	public void Set_IsInGame(bool isInGame)
	{	m_IsInGame = isInGame;
	}
	public bool Get_IsInGame()
	{	return m_IsInGame;
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_mainScreen")
		{	OGameMgr.Instance.ShowPopup("SHomePopup");
		}

		else if (button.m_buttonId == "btn_resultScreen")
		{	OGameMgr.Instance.ShowPopup("SResultPopup");
		}

		//else if (button.m_buttonId == "reload_btn")
		//{	PlayerScript.instance.Set_PlayerBullets(PlayerConfig.PLAYER_BULLETS);
		//	PlayerScript.instance.ShowReload(false);
		//}
	}

	public override void OnButtonDown (OButton button)
	{
		Debug.Log("OnButtonUp: " + button);
		if (button.m_buttonId == "btn_pause")
		{	OGameMgr.Instance.SetPaused(true);
			OGameMgr.Instance.ShowPopup("SPausePopup");
		}
	}


	Vector3 m_posTouch;
	float m_swipeOffset = PlayerConfig.SWIPE_RELOAD_OFFSET;

	public void OnTouchReload(Vector3 position)
	{
	}

	public void OnTouchUpReload(Vector3 position)
	{
		if( position.y < - 5 )
		{
			if( position.x < m_posTouch.x - m_swipeOffset )
					SwipeLeft(true);
				
			if( position.x > m_posTouch.x + m_swipeOffset )
					SwipeRight(true);
		}
		else
		{
			//Debug.Log("NOT RELOADING");
		}
	}

	public void OnTouchDownReload(Vector3 position)
	{
		if( position.y < - 5 )
		{
			m_posTouch = position;
		}
	}

	public void Update () 
	{
		if(!m_IsInGame)
			return;

		int MULTITOUCH_COUNT = Input.touchCount;
		if (MULTITOUCH_COUNT <= 0)
			MULTITOUCH_COUNT = 1;
		
		for(int i=0; i < OInputMgr.Instance.getTouchCount(); i++)
		{	if ( ( !Application.isEditor && Input.GetTouch(i).phase == TouchPhase.Began ) || 
			      ( Application.isEditor && Input.GetMouseButtonDown(0) ) ) 
			{
				//Debug.Log ( " Began : " + OInputMgr.Instance.getTouchDownPos(i));
				OnTouchDownReload (OInputMgr.Instance.getTouchDownPos(i));
			}
			
			if ( ( !Application.isEditor && Input.GetTouch(i).phase == TouchPhase.Ended ) || 
			    ( Application.isEditor && Input.GetMouseButtonUp(0) ) ) 
			{
				//Debug.Log ( " Ended : " + OInputMgr.Instance.getTouchUpPos(i));
				OnTouchUpReload (OInputMgr.Instance.getTouchUpPos(i));
			}
			
			if ( ( !Application.isEditor && Input.GetTouch(i).phase != TouchPhase.Canceled ) || 
			    ( Application.isEditor && Input.GetMouseButton(0) ) ) 
			{	
				//Debug.Log ( " Cancelled : " + OInputMgr.Instance.getTouchPos(i));
				OnTouchReload (OInputMgr.Instance.getTouchPos(i));
			}
		}
	}

	public void SwipeLeft(bool isLeft)
	{
		//Debug.Log ("Swipe Left");
		PlayerReload();
	}
	
	public void SwipeRight(bool isLeft)
	{
		//Debug.Log ("Swipe Right");
		PlayerReload();
	}

	void PlayerReload()
	{
		if(!PlayerScript.instance.Get_IsAlivePlayer())
			return;

		if(GridManager.instance.Get_IsGameStart())
		{
			if (PlayerScript.instance.Get_PlayerBullets () == 0)
			{
				if(PlayerScript.instance.Get_PlayerMultiplier() < 4)
					PlayerScript.instance.Set_PlayerMultiplier (PlayerScript.instance.Get_PlayerMultiplier () + 1, false);
			}
			else
			{
				PlayerScript.instance.Set_PlayerMultiplier (1, true);
			}
		}

		if(!GridManager.instance.Get_IsGameStart())
		{
			GridManager.instance.Set_IsGameStart(true);
			GridManager.instance.SetSpawnRound(GridManager.instance.Get_CurrentRound());
		}

		PlayerScript.instance.Set_PlayerBullets(PlayerConfig.PLAYER_BULLETS);
		PlayerScript.instance.ShowReload(false);
		PlayerScript.instance.ShowSwipeReload (false);
		OAudioMgr.Instance.PlaySFX (m_sfx_reload);
	}



}
