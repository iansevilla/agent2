﻿using UnityEngine;
using System.Collections;

public class SHomePopup : OPopupScene {

	public AudioClip m_bgm_Main;
	public AudioClip m_bgm_InGame;

	public OSpriteToggleButton m_toggleBGM;
	public OSpriteToggleButton m_toggleSFX;

	void Start()
	{
		if( !ODBMgr.Instance.getBool (ODBKeys.IS_APP_FIRST_OPEN) )
		{
			ODBMgr.Instance.setBool (ODBKeys.IS_APP_FIRST_OPEN, true);
			ODBMgr.Instance.setInt	(ODBKeys.GAME_BEST_SCORE, 0);

			ODBMgr.Instance.setBool (ODBKeys.GAME_FINISH_TUTORIAL, false);
		}
	}

	void OnEnable()
	{
		OAudioMgr.Instance.PlayBGM (m_bgm_Main);

		m_toggleBGM.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_BGM_ENABLED ) );
		m_toggleSFX.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_SFX_ENABLED ) );

		OAudioMgr.Instance.SetEnabledBGM (m_toggleBGM.m_isToggled);
		OAudioMgr.Instance.SetEnabledSFX (m_toggleSFX.m_isToggled);
		SGameScene.instance.Set_IsInGame (false);
	}

	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_play")
		{	
			if( !ODBMgr.Instance.getBool (ODBKeys.GAME_FINISH_TUTORIAL) )
			{
				ODBMgr.Instance.setBool (ODBKeys.GAME_FINISH_TUTORIAL, true);
				OGameMgr.Instance.ShowPopup("STutorialPopup");
			}
			else
			{
				PlayerScript.instance.Set_IsAlivePlayer(true);
				PlayerScript.instance.ResetPlayerScore();
				PlayerScript.instance.ShowReload(false);
				PlayerScript.instance.ShowSwipeReload(true);
				PlayerScript.instance.HideAllBullet();
				GridManager.instance.ResetGrid();
				GridManager.instance.Set_CurrentRound(0);
				OAudioMgr.Instance.PlayBGM (m_bgm_InGame);
				SGameScene.instance.Set_IsInGame (true);
				OGameMgr.Instance.HidePopup();

				//OInMobiHandler.Instance.removeBannerAd();
			}
		}

		else if (button.m_buttonId == "btn_bgm")
		{	OAudioMgr.Instance.SetEnabledBGM (m_toggleBGM.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_BGM_ENABLED, m_toggleBGM.m_isToggled );
		}

		else if (button.m_buttonId == "btn_sfx")
		{	OAudioMgr.Instance.SetEnabledSFX (m_toggleSFX.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_SFX_ENABLED, m_toggleSFX.m_isToggled );
		}

		else if (button.m_buttonId == "btn_aboutScreen")
		{	OGameMgr.Instance.ShowPopup("SAboutPopup");
		}

		else if (button.m_buttonId == "btn_tut")
		{	GlobalClass.instance.Set_IsFromMainMenu(true);
			OGameMgr.Instance.ShowPopup("STutorialPopup");
		}

		else if (button.m_buttonId == "btn_debug")
		{	OGameMgr.Instance.ShowPopup("SDebugPopup");
		}
	}
}
