﻿using UnityEngine;
using System.Collections;

public class STutorialPopup : OPopupScene {
	
	//Audio
	public AudioClip m_bgm_InGame;

	public GameObject m_GoTutorial;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnEnable()
	{
		m_GoTutorial.transform.position = new Vector2 (0, 0);
	}

	public override void OnButtonUp (OButton button)
	{

		if (button.m_buttonId == "btn_skip")
		{
			if(GlobalClass.instance.Get_IsFromMainMenu())
			{
				OGameMgr.Instance.ShowPopup("SHomePopup");
			}
			else
			{
				PlayerScript.instance.Set_IsAlivePlayer(true);
				PlayerScript.instance.ResetPlayerScore();
				PlayerScript.instance.ShowReload(false);
				PlayerScript.instance.ShowSwipeReload(true);
				PlayerScript.instance.HideAllBullet();
				GridManager.instance.ResetGrid();
				GridManager.instance.Set_CurrentRound(0);
				OAudioMgr.Instance.PlayBGM (m_bgm_InGame);
				SGameScene.instance.Set_IsInGame (true);
				OGameMgr.Instance.HidePopup();

				//OInMobiHandler.Instance.removeBannerAd();
			}
			GlobalClass.instance.Set_IsFromMainMenu(false);
		}
		else if (button.m_buttonId == "btn_back1")
		{
			OGameMgr.Instance.ShowPopup("SHomePopup");
			GlobalClass.instance.Set_IsFromMainMenu(false);
		}
		else if (button.m_buttonId == "btn_next1")
		{
			LeanTween.moveX (m_GoTutorial , -18f, .7f).setEase (LeanTweenType.easeOutCirc);
		}
		else if (button.m_buttonId == "btn_back2")
		{
			LeanTween.moveX (m_GoTutorial , 0f, .7f).setEase (LeanTweenType.easeOutCirc);
		}
		else if (button.m_buttonId == "btn_next2")
		{
			LeanTween.moveX (m_GoTutorial , -36f, .7f).setEase (LeanTweenType.easeOutCirc);
		}
		else if (button.m_buttonId == "btn_back3")
		{
			LeanTween.moveX (m_GoTutorial , -18f, .7f).setEase (LeanTweenType.easeOutCirc);
		}
		else if (button.m_buttonId == "btn_play")
		{
			PlayerScript.instance.Set_IsAlivePlayer(true);
			PlayerScript.instance.ResetPlayerScore();
			PlayerScript.instance.ShowReload(false);
			PlayerScript.instance.ShowSwipeReload(true);
			PlayerScript.instance.HideAllBullet();
			GridManager.instance.ResetGrid();
			GridManager.instance.Set_CurrentRound(0);
			OAudioMgr.Instance.PlayBGM (m_bgm_InGame);
			SGameScene.instance.Set_IsInGame (true);
			OGameMgr.Instance.HidePopup();

			//OInMobiHandler.Instance.removeBannerAd();
			GlobalClass.instance.Set_IsFromMainMenu(false);
		}
	}


}
