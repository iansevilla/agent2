﻿using UnityEngine;
using System.Collections;

public class SPausePopup : OPopupScene {

	public OLabel lblScore;
	public OLabel lblBestScore;
	
	public OSpriteToggleButton m_toggleBGM;
	public OSpriteToggleButton m_toggleSFX;

	// Use this for initialization
	void Start () {
	
	}

	void OnEnable()
	{
		//OInMobiHandler.Instance.createBannerAd();

		m_toggleBGM.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_BGM_ENABLED ) );
		m_toggleSFX.Toggle( ODBMgr.Instance.getBool( ODBKeys.GAME_SFX_ENABLED ) );

		lblScore.UpdateLabel (PlayerScript.instance.Get_PlayerScore ());
		lblBestScore.UpdateLabel ("Best: " + ODBMgr.Instance.getInt (ODBKeys.GAME_BEST_SCORE) + "pts.");
	}

	// Update is called once per frame
	void Update () {

	}


	public override void OnButtonUp (OButton button)
	{
		if (button.m_buttonId == "btn_resume")
		{	OGameMgr.Instance.SetPaused(false);
			OGameMgr.Instance.HidePopup();

		//	OInMobiHandler.Instance.removeBannerAd();
		}

		else if (button.m_buttonId == "btn_MainMenu")
		{	
			OGameMgr.Instance.SetPaused(false);
			GridManager.instance.Set_IsGameStart(false);
			PlayerScript.instance.Set_IsAlivePlayer(false);
			SGameScene.instance.Set_IsInGame (false);
			OGameMgr.Instance.ShowPopup("SHomePopup");
		}
		
		else if (button.m_buttonId == "btn_bgm")
		{	OAudioMgr.Instance.SetEnabledBGM (m_toggleBGM.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_BGM_ENABLED, m_toggleBGM.m_isToggled );
		}
		
		else if (button.m_buttonId == "btn_sfx")
		{	OAudioMgr.Instance.SetEnabledSFX (m_toggleSFX.m_isToggled);
			ODBMgr.Instance.setBool( ODBKeys.GAME_SFX_ENABLED, m_toggleSFX.m_isToggled );
		}
	}


}
