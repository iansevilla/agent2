﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerScript : MonoBehaviour {

	public static PlayerScript instance;

	//Label
	public OLabel m_lblPlayerScore;
	public OLabel m_lblPlayerBullet;
	public OLabel m_lblMultiplier;
	public OLabel m_lblBestScore;
	
	//Player Variable
	private bool m_bIsAlivePlayer;
	private int	m_nPlayerBullets;
	private int m_nPlayerScore;
	private int m_nPlayerBest;

	private int m_nPlayerMultiplier;
	private bool m_bIsFromMissed = false;

	public SpriteRenderer m_BulletSprite1;
	public SpriteRenderer m_BulletSprite2;
	public SpriteRenderer m_BulletSprite3;
	public SpriteRenderer m_BulletSprite4;
	public SpriteRenderer m_BulletSprite5;
	public SpriteRenderer m_BulletSprite6;

	[HideInInspector]
	public List<SpriteRenderer> bulletList = new List<SpriteRenderer>();

	//Reload
	public GameObject m_goReload;
	public GameObject m_goReloadSwipe;

	void Start () {

		instance 				= this;
		m_bIsAlivePlayer 		= true;
		m_nPlayerBullets 		= PlayerConfig.PLAYER_BULLETS;

		bulletList.Add (m_BulletSprite1);
		bulletList.Add (m_BulletSprite2);
		bulletList.Add (m_BulletSprite3);
		bulletList.Add (m_BulletSprite4);
		bulletList.Add (m_BulletSprite5);
		bulletList.Add (m_BulletSprite6);

		for(int i = 0; i < bulletList.Count; i++)
		{
			bulletList[i].renderer.enabled = false;
		}
	}

	void Update () {
		m_nPlayerBest = ODBMgr.Instance.getInt(ODBKeys.GAME_BEST_SCORE);
		if(m_nPlayerBest > 0)
			m_lblBestScore.UpdateLabel ("Best: " + m_nPlayerBest);
		else
			m_lblBestScore.UpdateLabel (" ");
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// PlayerAlive ////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public bool Get_IsAlivePlayer()
	{	return m_bIsAlivePlayer;
	}

	public void Set_IsAlivePlayer(bool isAlive)
	{	m_bIsAlivePlayer = isAlive;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// PlayerBullets //////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public int Get_PlayerScore()
	{	return m_nPlayerScore;
	}

	public void Set_PlayerScore(int nGainScore)
	{	m_nPlayerScore += nGainScore;
		m_lblPlayerScore.UpdateLabel ("Score: " + m_nPlayerScore);
		if(ODBMgr.Instance.getInt(ODBKeys.GAME_BEST_SCORE) < m_nPlayerScore)
			ODBMgr.Instance.setInt (ODBKeys.GAME_BEST_SCORE, m_nPlayerScore);
	}

	public void ResetPlayerScore()
	{	
		m_nPlayerScore = 0;
		m_nPlayerBullets = 0;//PlayerConfig.PLAYER_BULLETS;
		m_nPlayerMultiplier = 1;

		m_lblPlayerScore.UpdateLabel ("Score: " + m_nPlayerScore);
		m_lblPlayerBullet.UpdateLabel ("Bullet: " + m_nPlayerBullets);
		m_lblMultiplier.UpdateLabel (m_nPlayerMultiplier + "x" );
	}

	public void ShowReload(bool isVisible)
	{	m_goReload.SetActive (isVisible);
		//PlayerScript.instance.Set_PlayerMultiplier (1);
	}

	public void ShowSwipeReload(bool isVisible)
	{	m_goReloadSwipe.SetActive(isVisible);
	}
	
	public bool Get_IsPlayerOutOfBullet()
	{	if(m_nPlayerBullets <= 0)
			return true;
		else
			return false;
	}

	public int Get_PlayerBullets()
	{	return m_nPlayerBullets;
	}

	public void Set_PlayerBullets(int nRemainingBullets)
	{	m_nPlayerBullets = nRemainingBullets;

		if(nRemainingBullets == PlayerConfig.PLAYER_BULLETS)
		{
			StartCoroutine(ReloadBulletAnim());
		}
		else
		{
			for(int i = 0; i < bulletList.Count; i++)
			{
				bulletList[i].renderer.enabled = false;
			}

			for(int i = 0; i < nRemainingBullets; i++)
			{
				bulletList[i].renderer.enabled = true;
			}
		}

		m_lblPlayerBullet.UpdateLabel ("Bullet: " + m_nPlayerBullets);
		if(m_nPlayerBullets == 0)
			ShowSwipeReload(true);
	}

	IEnumerator ReloadBulletAnim()
	{
		for(int i = 0; i < bulletList.Count; i++)
		{
			bulletList[i].renderer.enabled = true;
			yield return new WaitForSeconds(.05f);
		}
	}

	public void HideAllBullet()
	{
		for(int i = 0; i < bulletList.Count; i++)
		{
			bulletList[i].renderer.enabled = false;
		}
	}

	public void SetIsFromMissed(bool isMissed)
	{	m_bIsFromMissed = isMissed;
	}


	public void Set_PlayerMultiplier(int nMultiplier, bool isMissed)
	{
		if(m_nPlayerMultiplier < 4)
		{
			m_nPlayerMultiplier = nMultiplier;
		}

		if(m_bIsFromMissed)
		{	m_bIsFromMissed = false;
			m_nPlayerMultiplier = 1;
		}

		if(isMissed)
		{
			m_bIsFromMissed = isMissed;
			m_nPlayerMultiplier = nMultiplier;
			Debug.Log("MISSED MISSED MISSED: " + m_bIsFromMissed);
		}

		m_lblMultiplier.UpdateLabel (m_nPlayerMultiplier + "x");
	}

	public int Get_PlayerMultiplier()
	{	return m_nPlayerMultiplier;
	}

	public void Set_PlayerBestScore(int nBestScore)
	{	m_nPlayerBest = nBestScore;
		m_lblBestScore.UpdateLabel ("Best: " + nBestScore);
	}
}
