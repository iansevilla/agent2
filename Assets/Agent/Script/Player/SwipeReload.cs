﻿using UnityEngine;
using System.Collections;

public class SwipeReload : MonoBehaviour {

	// Use this for initialization
	void Start () {
		ShowSwipeReload ();
	}

	void ShowSwipeReload()
	{
		this.transform.position = new Vector2 (-15, this.transform.position.y);
		LeanTween.moveX (this.gameObject, 0f, .7f).setEase (LeanTweenType.easeOutCirc).setOnComplete (SwipeReloadOut).setDelay (1f);
	}
	void SwipeReloadOut()
	{
		LeanTween.moveX (this.gameObject, 15f, .7f).setEase (LeanTweenType.easeInCirc).setOnComplete (ShowSwipeReload);
	}
}
