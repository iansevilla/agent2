﻿using UnityEngine;
using System.Collections;

public class ScoringText : MonoBehaviour {

	public static ScoringText instance;

	private float m_upSpeed = 30f;
	private string m_Score;

	public OLabel m_lblScore;

	public float posY = 0;

	// Use this for initialization
	void Start () {
		instance = this;
		posY = this.transform.position.y + .5f;
	}
	
	// Update is called once per frame
	void Update () {
		m_lblScore.UpdateLabel ("+" + m_Score);
		if(posY > this.transform.position.y)
			this.rigidbody2D.velocity = Vector3.up * Time.deltaTime * m_upSpeed;
		else
			this.rigidbody2D.velocity = Vector3.zero;

	
	}
	public void Set_Score(int sScore)
	{
		m_Score = sScore.ToString();
	}

}
