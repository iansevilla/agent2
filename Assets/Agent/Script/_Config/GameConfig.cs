﻿public class GameConfig 
{
	public const float SPAWN_TIMER							= 0.5f;
	public const float GRID_RESET_TIMER						= 3f;


	//			7
	//			3
	//			-1
	//		Y	-5
	//				
	//			X	-4	-1	2	5

	public const int GRID_X_1								= -4;
	public const int GRID_X_2								= -1;
	public const int GRID_X_3								= 2;
	public const int GRID_X_4								= 5;

	public const int GRID_Y_1								= 7;
	public const int GRID_Y_2								= 3;
	public const int GRID_Y_3								= -1;
	public const int GRID_Y_4								= -5;

}

public class AgentConfig 
{
	public const float 	AGENT_SHOOT_SPEED 					= 2f;
	public const float 	AGENT_MAX_SHOOT_SPEED 				= 2f;

	public const int	AGENT_SCORE							= 1;
	public const int	AGENT_HEADSHOOT_SCORE				= 10;
}

public class CivilConfig
{
	public const float CIVIL_APPERANCE_TIME 				= 3f;
	public const int CIVIL_SCORE							= -10;
}


public class PlayerConfig
{
	public const int	PLAYER_BULLETS						= 6;
	public const float	SWIPE_RELOAD_OFFSET					= 2f;
}
